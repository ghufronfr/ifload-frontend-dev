'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var merge = require('merge-stream');

var SCSS_SRC = ['./src/styles/scss/*.scss', './src/components/styles/scss/*.scss'];
var SCSS_DEST = ['./src/styles/css', './src/components/styles/css'];

gulp.task('build_sass', function() {
    var tasks = SCSS_SRC.map(function(source, i){
        return gulp.src(source)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(SCSS_DEST[i]));
    });
    return merge(tasks);
});

gulp.task('watch_sass', function() {
    gulp.watch(SCSS_SRC, ['build_sass'])
});

gulp.task('default', ['watch_sass']);