import { combineReducers } from 'redux'

import PostList from './PostList'

export default combineReducers({
    PostList
})