export default function reducer(state={
    posts: [],
    fectching: false,
    fetched: false,
    error: null
}, action) {
    switch(action.type) {
        case 'FETCH_POSTS' : {
            return {...state, fectching: true}
        }
        case 'FETCH_POSTS_FULFILLED' : {
            return {...state, fectching: false, fetched: true, posts: action.payload}
            
        }
        case 'FETCH_POSTS_REJECTED': {
            return {...state, fectching: false, error: action.payload}
        }
        default: {
            
        }
    }
    return state
}