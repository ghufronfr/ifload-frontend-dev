
const theme = {
    primary: {
        normal: '#00a8ff',
        dark: '#008FE6'
    },
    secondary: {
        normal: '#eceeef',
        dark: '#D3D5D6'
    },
    danger: {
        normal: '#fa424a',
        dark: '#E12931'
    },
    light: '#fafafa',
    dark: '#343434',
    white: '#ffffff',
    black: '#000000',
    purple: '#ac6bec',
    media: {
        facebook: '#3b5998',
        twitter: '#2ba9e1',
        whatsapp: '#34AF23'
    }
}

export default theme