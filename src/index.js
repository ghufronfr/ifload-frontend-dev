import React from 'react'
import { render } from 'react-dom'
import registerServiceWorker from './registerServiceWorker'

import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store'

import Home from 'containers/Home'
import Login from 'containers/Login'

import 'sanitize.css/sanitize.css'
import 'font-awesome/css/font-awesome.min.css'
import 'styles/css/index.css'

import { ThemeProvider } from 'styled-components'
import theme from './styles/theme.js'


const target = document.querySelector('#root')

render (
	<Provider store ={store}>
		<ThemeProvider theme={theme}>
			<BrowserRouter>
				<Switch>
					<Route exact path='/' component={Home}/>
					<Route path='/login' component={Login}/>
				</Switch>        
			</BrowserRouter>
		</ThemeProvider>
	</Provider>, target
)

registerServiceWorker()
