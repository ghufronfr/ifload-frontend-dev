import React, { Component } from 'react'
import styled from 'styled-components'


export const Button = styled.button`
    background: ${props => props.theme[props.var].normal};
    color: #fafafa;
    padding: 9px 16px;
    border: none;
    cursor: pointer;
    border-radius: 3px;
    &:hover {
        background: ${props => props.theme[props.var].dark};
    }
`



