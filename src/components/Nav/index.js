import React, { Component } from 'react'
import styled from 'styled-components'

export const Nav = styled.div`
    background-color: #fafafa;
    padding: 0 15px;
    border-bottom: 1px solid #c5d6de;

    @media (max-width: 1056px) {
        .form-inline {
            visibility: collapse;
        };
        display: none;
        visibility: hidden;
    }

    & ul {
        list-style-type: none;
        padding:0;
        margin:0;

        & li {
            display: inline-block;
            height:56px;
            margin: 0 8px;
            & a {
                display: block;
                font-weight: 500;
                font-size: 15px;
                padding: 20px 0;
                & i {
                    margin-right: 3px;
                }
            }
            &:hover a{
                border-bottom: 4px solid ${props => props.theme.primary.normal};
                padding-bottom: 14px;
                color: #343434;
            }
        }
    }
`
export const Navbar = styled.div`
    background-color: #fafafa;
    padding: 15px 35px 15px 15px;
    border-bottom: 1px solid #c5d6de;
    display: flex;    
`

export const Logo = styled.img`
    height: 50px;
    float: left;
`

