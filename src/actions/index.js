import axios from 'axios'

export function fetchPost() {
    return function(dispatch) {
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then((response) => {
            dispatch({type:'FECTH_POSTS_FULLFILLED', payload:response.data})
        })
        .catch((err) => {
            dispatch({type:'FECTH_POSTS_REJECTED', payload:err})
        })
    }
}