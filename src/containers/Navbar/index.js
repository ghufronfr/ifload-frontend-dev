import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { MdCloudUpload, MdSearch } from 'react-icons/lib/md'
import { Nav, Navbar, Logo } from 'components/Nav'
import { Button } from 'components/Button'

const Wrapper = styled.div`
    position: fixed;
    width: 100%;
`
const Form = styled.input`
    background-color: #eaeaea;
    padding: 10.5px 8px;
    margin: 0 126px;
    float: left;
    width: 100%;
`
const Bar = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
`

const navList = {
    'Acak': {link: '#random', icon: 'fa-random'},
    'Berkas Populer': {link: '#popular', icon: 'fa-fire'},
    'Berkas Terbaru': {link: '#newest', icon: 'fa-th-large'},
    'Musik': {link: '#music', icon: 'fa-music'},
    'Video': {link: '#video', icon: 'fa-youtube-play'},
    'Software': {link: '#software', icon: 'fa-laptop'},
    'Aplikasi': {link: '#application', icon: 'fa-mobile'},
    'Lain-Lain': {link: '#other', icon: 'fa-clone'},
    'Leaderboard': {link: '#other', icon: 'fa-users'}
}

export default class Header extends Component {
    render () {
        const navItem = Object.keys(navList)
        .map((item) => <li key={item}><Link to={navList[item].link}> <i className={'fa ' + navList[item].icon }></i>{item}</Link></li>)
        
        return (
            <Wrapper>
                <Navbar>
                    <Logo src='./img/logo.png'/>
                    <Bar>
                        <Form placeholder='Cari'/>
                        <Button var='danger'><i className='fa fa-cloud-upload'></i></Button>
                        <Button var='primary' style={{marginLeft:16}}>Masuk</Button>                        
                    </Bar>
                </Navbar>
                <Nav>
                    <ul>
                        {navItem}
                    </ul>
                </Nav>
            </Wrapper>
        )
    }
}
