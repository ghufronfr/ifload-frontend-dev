import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const styles = {
    width: '98%',
    height: 40
}
export default class Login extends Component {
    render () {
        return (
            <section>
                <div className='navbar navbar-light bg-light navbar-top'>
                    <div className='container-fluid pt-2'>
                    <Link to='/'><img className='site-logo-img' src='./img/logo.png' alt='logo'/></Link>
                    </div>
                </div>
                <div className='container'>
                    <div className='col-md-3 ml-auto mr-auto'>
                        <div className='card'>
                            <div className='card-body'>
                                <img src='./img/avatar-sign.png' alt='avatar'/>
                                <h2>Masuk IFload</h2>
                                <div className='bg-primary' style={styles}> 
                                </div>
                                <p>Baru di website kami? <span><a href=''> Baca Dokumentasi</a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        )
    }
   
}